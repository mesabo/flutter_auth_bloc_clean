import 'package:get/get.dart';

import '../../features/authentication/presentation/pages/view/home_screen.dart';
import '../../features/authentication/presentation/pages/view/login_screen.dart';
import '../../features/authentication/presentation/pages/view/splash_screen.dart';

class RoutesProvider {
  static const String splah = "/";
  static const String home = "/home";
  static const String login = "/login";

  static List<GetPage> routes = [
    GetPage(name: splah, page: () => const SplashScreen()),
    GetPage(name: home, page: () => const HomeScreen()),
    GetPage(name: login, page: () => const LoginScreen()),
  ];
}
