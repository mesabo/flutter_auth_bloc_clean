class AppConstants {
  static const String networkError =
      "Impossible de se connecter au serveur. Veuillez vérifier votre connexion Internet.";
  static const String app_local_db = "app_local_db";
  static const String base_url = "http://192.168.68.116:3333/api/v1";
  static const String authKey = "auth";
  static const int pass_code_valid_input_length = 4;
}
