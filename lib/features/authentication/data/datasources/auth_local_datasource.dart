import 'dart:convert';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:project_bloc_clean/core/error/exceptions.dart';
import 'package:project_bloc_clean/core/providers/hive_helper.dart';
import 'package:project_bloc_clean/core/utils/app_constants.dart';
import 'package:project_bloc_clean/features/authentication/data/models/auth_model.dart';

abstract class AuthLocalDatasource {
  Future<AuthModel> getAuthFromCache();
  Future<void> cacheAuth(AuthModel authModel);
  Future<String> deleteAuthCache();
}

class AuthLocalDatasourceImpl extends AuthLocalDatasource {
  final HiveInterface hive;

  AuthLocalDatasourceImpl({required this.hive});

  @override
  Future<void> cacheAuth(AuthModel authModel) async {
    try {
      HiveHelper helper = HiveHelper(hive: hive);
      final String autString = json.encode(authModel.toMap());
      await helper.setValue(AppConstants.authKey, autString);
      String response = helper.getValue(AppConstants.authKey);
      print('fkfjfsjkfjkfsd');
      print(response);
    } catch (e) {
      ///TODO CacheFailure ou CacheException ?
      throw CacheException();
    }
  }

  @override
  Future<AuthModel> getAuthFromCache() async {
    try {
      HiveHelper helper = HiveHelper(hive: hive);
      String response = helper.getValue(AppConstants.authKey);
      print('responseeeee');
      print(response);
      final toAuth = json.decode(response);
      Map<String, dynamic> responseJson = json.decode(toAuth);

      return AuthModel.fromMap(responseJson);
    } catch (e) {
      print(e);
      throw CacheException;
    }
  }

  @override
  Future<String> deleteAuthCache() async {
    try {
      HiveHelper helper = HiveHelper(hive: hive);
      return await helper.deleteValue(AppConstants.authKey);
    } catch (e) {
      throw CacheException;
    }
  }
}
