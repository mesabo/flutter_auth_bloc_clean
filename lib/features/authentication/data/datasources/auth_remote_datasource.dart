import 'package:dio/dio.dart';
import 'package:project_bloc_clean/core/error/exceptions.dart';
import 'package:project_bloc_clean/core/providers/api_helper.dart';
import 'package:project_bloc_clean/core/usecases/usecases.dart';
import 'package:project_bloc_clean/core/utils/app_constants.dart';
import 'package:project_bloc_clean/features/authentication/data/models/signin_payload.dart';

import '../models/auth_model.dart';

abstract class AuthRemoteDatasource {
  Future<AuthModel> signin(SigninPayload signinPayload);
  Future<String> logout(String token);
}

class AuthRemoteDatasourceImpl extends AuthRemoteDatasource {
  AuthRemoteDatasourceImpl({required this.dio});
  final Dio dio;

  @override
  Future<AuthModel> signin(SigninPayload signinPayload) async {
    dio..options.connectTimeout = 150000;
    try {
      final response =
          await APIHelper(dio: dio, apiBaseUrl: AppConstants.base_url)
              .post('/login', body: signinPayload.toJson());
      AuthModel authModel = AuthModel(
          token: response['token']['token'],
          userId: response['user']['user_id'],
          firstName: response['user']['first_name'],
          lastName: response['user']['last_name'],
          phoneNumber: response['user']['phone_number'],
          emailAddress: response['user']['email_address']);
      return authModel;
    } on DioError catch (e) {
      throw RequestException.fromDioError(e);
    } on ServerException catch (e) {
      print('dddd');
      throw ServerException();
    }
  }

  @override
  Future<String> logout(String token) async {
    dio..options.connectTimeout = 150000;
    try {
      final response =
          await APIHelper(dio: dio, apiBaseUrl: AppConstants.base_url)
              .post('/logout', headers: {'Authorization': 'Bearer $token'});

      return "Déconnexion effectuée avec succès";
    } on DioError catch (e) {
      throw RequestException.fromDioError(e);
    } on ServerException catch (e) {
      print('dddd');
      throw ServerException();
    }
  }
}
