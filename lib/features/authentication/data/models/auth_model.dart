import 'dart:convert';

import 'package:project_bloc_clean/features/authentication/domain/entities/auth.dart';

class AuthModel extends Auth {
  const AuthModel(
      {required super.token,
      required super.userId,
      required super.firstName,
      required super.lastName,
      required super.phoneNumber,
      required super.emailAddress});

  factory AuthModel.fromMap(Map<String, dynamic> jsonMap) => AuthModel(
      token: jsonMap['token'],
      userId: jsonMap['userId'],
      firstName: jsonMap['firstName'],
      lastName: jsonMap['lastName'],
      phoneNumber: jsonMap['phoneNumber'],
      emailAddress: jsonMap['emailAddress']);

  Map<String, dynamic> toMap() {
    return {
      'token': token,
      'userId': userId,
      'firstName': firstName,
      'lastName': lastName,
      'phoneNumber': phoneNumber,
      'emailAddress': emailAddress,
    };
  }

}
