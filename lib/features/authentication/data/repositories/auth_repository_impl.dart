import 'package:get/get.dart';
import 'package:project_bloc_clean/core/error/exceptions.dart';
import 'package:project_bloc_clean/core/utils/app_constants.dart';
import 'package:project_bloc_clean/features/authentication/data/models/auth_model.dart';
import 'package:project_bloc_clean/features/authentication/domain/entities/auth.dart';
import 'package:project_bloc_clean/features/authentication/data/models/signin_payload.dart';
import 'package:project_bloc_clean/core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:project_bloc_clean/features/authentication/domain/repositories/auth_repository.dart';

import '../../../../core/platform/network/network_info.dart';
import '../datasources/auth_local_datasource.dart';
import '../datasources/auth_remote_datasource.dart';

class AuthRepositoryImp extends AuthRepository {
  AuthRepositoryImp(
      {required this.authLocalDataSource,
      required this.authRemoteDataSource,
      required this.networkInfo});

  final AuthLocalDatasource authLocalDataSource;
  final AuthRemoteDatasource authRemoteDataSource;
  final NetworkInfo networkInfo;

  @override
  Future<Either<Failure, Auth>> getAuthFromCache() async {
    try {
      final AuthModel authModel = await authLocalDataSource.getAuthFromCache();
      return Right(authModel);
    } catch (e) {
      return Left(CacheFailure(message: "Cache is empty"));
    }
  }

  @override
  Future<Either<Failure, Auth>> signIn(SigninPayload login) async {
    if (await networkInfo.isConnected) {
      try {
        final AuthModel response = await authRemoteDataSource.signin(login);
        authLocalDataSource.cacheAuth(response);
        return Right(response);
      } on RequestException catch (e) {
        return Left(
            ServerFailure(statusCode: e.statusCode!, message: e.message));
      }
    } else {
      return Left(
          ServerFailure(statusCode: -1, message: AppConstants.networkError));
    }
  }

  @override
  Future<Either<Failure, String>> logout() async {
    if (await networkInfo.isConnected) {
      try {
        final token = await authLocalDataSource.getAuthFromCache();
        printInfo(info: '${token.toMap()  }');
        await authRemoteDataSource.logout(token.token);
        final response = await authLocalDataSource.deleteAuthCache();
        return Right(response);
      } on RequestException catch (error) {
        return Left(ServerFailure(
          statusCode: error.statusCode!,
          message: error.message,
          response: error.response,
        ));
      }
    } else {
      try {
        final response = await authLocalDataSource.deleteAuthCache();
        return Right(response);
      } on CacheException {
        return Left(CacheFailure(message: "Suppression du token du cache"));
      }
    }
  }
}
