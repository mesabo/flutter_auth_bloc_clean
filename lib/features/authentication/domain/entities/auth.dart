import 'package:equatable/equatable.dart';

class Auth extends Equatable {
  const Auth(
      {required this.token,
      required this.userId,
      required this.firstName,
      required this.lastName,
      required this.phoneNumber,
      required this.emailAddress});

  final String token;
  final int userId;
  final String firstName;
  final String lastName;
  final String phoneNumber;
  final String emailAddress;

  @override
  List<Object?> get props =>
      [token, userId, firstName, lastName, phoneNumber, emailAddress];
}
