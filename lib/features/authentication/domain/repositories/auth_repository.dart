import 'package:dartz/dartz.dart';
import 'package:project_bloc_clean/core/error/failure.dart';
import 'package:project_bloc_clean/features/authentication/data/models/signin_payload.dart';
import 'package:project_bloc_clean/features/authentication/domain/entities/auth.dart';

abstract class AuthRepository {
  Future<Either<Failure, Auth>> signIn(SigninPayload login);
  Future<Either<Failure, Auth>> getAuthFromCache();
  Future<Either<Failure, String>> logout();
}
