import 'package:project_bloc_clean/core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:project_bloc_clean/core/usecases/usecases.dart';

import '../entities/auth.dart';
import '../repositories/auth_repository.dart';

class GetAuthFromCache extends UseCase<Auth, NoParams> {
  GetAuthFromCache(this.authReposiotory);// TODO cacat +Uscase

  final AuthRepository authReposiotory;

  @override
  Future<Either<Failure, Auth>> call(NoParams params) async {
    return await authReposiotory.getAuthFromCache();
  }
}
