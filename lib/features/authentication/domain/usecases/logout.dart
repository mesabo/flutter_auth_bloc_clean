import 'package:project_bloc_clean/core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:project_bloc_clean/core/usecases/usecases.dart';
import 'package:project_bloc_clean/features/authentication/data/models/signin_payload.dart';
import 'package:project_bloc_clean/features/authentication/domain/repositories/auth_repository.dart';

import '../entities/auth.dart';

class LogoutUsecaseImpl extends UseCase<String, NoParams> {
  LogoutUsecaseImpl(this.authReposiotory);

  final AuthRepository authReposiotory;

  @override
  Future<Either<Failure, String>> call(NoParams params) async {
    return await authReposiotory.logout();
  }
}
