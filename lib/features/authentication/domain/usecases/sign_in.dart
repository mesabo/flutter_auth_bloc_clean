import 'package:project_bloc_clean/core/error/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:project_bloc_clean/core/usecases/usecases.dart';
import 'package:project_bloc_clean/features/authentication/data/models/signin_payload.dart';
import 'package:project_bloc_clean/features/authentication/domain/repositories/auth_repository.dart';

import '../entities/auth.dart';

class SigninUsecaseImpl extends UseCase<Auth, SigninPayload> {
  SigninUsecaseImpl(this.authReposiotory);

  final AuthRepository authReposiotory;

  /// `Attention au type de retour de call qui depend du type de `
  /// `retour de signin`
  @override
  Future<Either<Failure, Auth>> call(SigninPayload params) async {
    return await authReposiotory.signIn(params);
  }
}
