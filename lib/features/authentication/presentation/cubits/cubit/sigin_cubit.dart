import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:project_bloc_clean/core/error/failure.dart';
import 'package:project_bloc_clean/core/usecases/usecases.dart';
import 'package:project_bloc_clean/features/authentication/data/models/signin_payload.dart';
import 'package:project_bloc_clean/features/authentication/domain/entities/auth.dart';
import 'package:project_bloc_clean/features/authentication/domain/usecases/get_auth_from_cache.dart';
import 'package:project_bloc_clean/features/authentication/domain/usecases/sign_in.dart';

import '../../../domain/usecases/logout.dart';

part 'sigin_state.dart';

class SiginCubit extends Cubit<SiginState> {
  SiginCubit(
      {required this.signinUsecaseImpl,
      required this.getAuthFromCache,
      required this.logoutUser})
      : super(SiginInitial());

  final SigninUsecaseImpl signinUsecaseImpl;
  final GetAuthFromCache getAuthFromCache;
  final LogoutUsecaseImpl logoutUser;

  login(SigninPayload payload) async {
    emit(SigInLoading());
    final Either<Failure, Auth> authResponse = await signinUsecaseImpl(payload);
    authResponse.fold((l) {
      emit(SiginFailed(messahe: l.message));
    }, (r) {
      emit(SiginSuccess(authUser: r));
    });
  }

  getAuth() async {
    emit(CacheLoading());
    final Either<Failure, Auth> loadAuth = await getAuthFromCache(NoParams());
    loadAuth.fold((l) {
      emit(CacheFailed(messahe: l.message));
    }, (r) {
      emit(CacheSuccess(authUser: r));
    });
  }

  logOut() async {
    emit(LogoutLoading());
    final Either<Failure, String> logoutResponse = await logoutUser(NoParams());
    logoutResponse.fold((l) {
      emit(LogoutFailed());
    }, (r) {
      emit(LogoutSuccess());
    });
  }
}
