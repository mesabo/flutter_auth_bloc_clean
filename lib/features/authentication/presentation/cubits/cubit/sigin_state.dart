part of 'sigin_cubit.dart';

abstract class SiginState extends Equatable {
  const SiginState();

  @override
  List<Object> get props => [];
}

class SiginInitial extends SiginState {}

class SigInLoading extends SiginState {}

class SiginSuccess extends SiginState {
  const SiginSuccess({required this.authUser});
  final Auth authUser;

  @override
  List<Object> get props => [authUser];
}

class SiginFailed extends SiginState {
  const SiginFailed({required this.messahe});
  final String messahe;
  @override
  List<Object> get props => [messahe];
}

//---------------------------//

class CacheLoading extends SiginState {}

class CacheSuccess extends SiginState {
  //TODO be carefull with SigninSuccess
  final Auth authUser;
  const CacheSuccess({required this.authUser});
  @override
  List<Object> get props => [authUser];
}

class CacheFailed extends SiginState {
  const CacheFailed({required this.messahe});
  final String messahe;
  @override
  List<Object> get props => [messahe];
}

class LogoutLoading extends SiginState {}

class LogoutSuccess extends SiginState {}

class LogoutFailed extends SiginState {}
