import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:project_bloc_clean/features/authentication/presentation/pages/view/login_screen.dart';

import '../../cubits/cubit/sigin_cubit.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Home Page'), actions: [
        BlocConsumer<SiginCubit, SiginState>(listener: (context, state) async {
          if (state is LogoutSuccess) {
            Get.off(() => const LoginScreen());
          }
        }, builder: (context, state) {
          if (state is LogoutLoading) {
            return const CircularProgressIndicator();
          } else {
            return IconButton(
                onPressed: () {
                  context.read<SiginCubit>().logOut();
                },
                icon: const Icon(Icons.logout));
          }
        })
      ]),
      body: SafeArea(
        child: Center(
          child: BlocBuilder<SiginCubit, SiginState>(
            builder: (authContext, state) {
              if (state is SiginSuccess) {
                return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Nom : ${state.authUser.firstName} '),
                      Text('Prenom : ${state.authUser.lastName} '),
                      Text('email : ${state.authUser.emailAddress} '),
                      Text('Telephone : ${state.authUser.phoneNumber} '),
                    ]);
              } else {
                return const Text('il n y a rien a voir');
              }
            },
          ),
        ),
      ),
    );
  }
}
