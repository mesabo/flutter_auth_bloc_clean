import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bloc_clean/features/authentication/presentation/cubits/cubit/sigin_cubit.dart';
import 'package:project_bloc_clean/features/authentication/presentation/pages/view/home_screen.dart';
import 'package:project_bloc_clean/features/authentication/presentation/pages/view/login_screen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: BlocListener<SiginCubit, SiginState>(
        listener: (authContext, state) {
          print(state);
          if (state is CacheSuccess) {
            Future.delayed(const Duration(seconds: 1), () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => const HomeScreen()));
            });
          } else if (state is CacheFailed) {
            Future.delayed(const Duration(seconds: 1), () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => const LoginScreen()));
            });
          }
        },
        child: const Center(child: CircularProgressIndicator()),
      )),
    );
  }
}
