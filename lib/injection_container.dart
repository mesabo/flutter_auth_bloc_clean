import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import 'core/platform/network/network_info.dart';
import 'core/providers/hive_helper.dart';
import 'features/authentication/data/datasources/auth_local_datasource.dart';
import 'features/authentication/data/datasources/auth_remote_datasource.dart';
import 'features/authentication/data/repositories/auth_repository_impl.dart';
import 'features/authentication/domain/repositories/auth_repository.dart';
import 'features/authentication/domain/usecases/get_auth_from_cache.dart';
import 'features/authentication/domain/usecases/logout.dart';
import 'features/authentication/domain/usecases/sign_in.dart';
import 'features/authentication/presentation/cubits/cubit/sigin_cubit.dart';

final sl = GetIt.instance;
Future<void> init() async {
  // App Features
  //BLOC

  // Business Features
  // CUBIT
  sl.registerFactory(() => SiginCubit(
      signinUsecaseImpl: sl(), getAuthFromCache: sl(), logoutUser: sl()));

  // Use Case
  sl.registerLazySingleton(() => SigninUsecaseImpl(sl()));
  sl.registerLazySingleton(() => GetAuthFromCache(sl()));
  sl.registerLazySingleton(() => LogoutUsecaseImpl(sl()));

  // Repository
  sl.registerLazySingleton<AuthRepository>(() => AuthRepositoryImp(
      authRemoteDataSource: sl(),
      authLocalDataSource: sl(),
      networkInfo: sl()));

  // Datasource
  sl.registerLazySingleton<AuthLocalDatasource>(
      () => AuthLocalDatasourceImpl(hive: sl()));
  sl.registerLazySingleton<AuthRemoteDatasource>(
      () => AuthRemoteDatasourceImpl(dio: sl()));

  // Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  // Providers
  sl.registerLazySingleton(() => HiveHelper(hive: sl()));

  // External
  final Dio dio = Dio();
  final HiveInterface hive = Hive;
  sl.registerLazySingleton(() => InternetConnectionChecker());
  sl.registerLazySingleton(() => dio);
  sl.registerLazySingleton(() => hive);
}
