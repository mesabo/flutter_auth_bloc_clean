import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:project_bloc_clean/core/providers/routes_provider.dart';
import 'package:project_bloc_clean/core/utils/app_constants.dart';
import 'package:project_bloc_clean/features/authentication/presentation/cubits/cubit/sigin_cubit.dart';
import 'injection_container.dart' as inject_conn;
import 'injection_container.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get_navigation/get_navigation.dart' as nav;

void main() async {
  await inject_conn.init();
  await Hive.initFlutter();
  Hive.openBox(AppConstants.app_local_db);
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => sl<SiginCubit>()..getAuth())
      ],
      child: GetMaterialApp(
        navigatorKey: Get.key,
        enableLog: true,
        defaultTransition: nav.Transition.cupertino,
        title: 'Authentication',
        debugShowCheckedModeBanner: false,
        initialRoute: RoutesProvider.splah,
        getPages: RoutesProvider.routes,
      ),
    );
  }
}
// 