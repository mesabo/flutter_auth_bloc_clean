import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:project_bloc_clean/features/authentication/data/models/auth_model.dart';
import 'package:project_bloc_clean/features/authentication/domain/entities/auth.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  test('should be a subclass Auth Entity', () {
    const authResponse = AuthModel(
        token: 'token',
        userId: 1,
        firstName: 'firstName',
        lastName: 'lastName',
        phoneNumber: 'phoneNumber',
        emailAddress: 'emailAddress');

    expect(authResponse, isA<Auth>());
  });

  test('should return data from map', () {
    /// ARRANGE
    const authResponse = AuthModel(
        token: 'token',
        userId: 1,
        firstName: 'toto',
        lastName: 'tat',
        phoneNumber: '0101020203',
        emailAddress: 'test@gmail.com');
    Map<String, dynamic> jsonMap = json.decode(fixture('auth.json'));

    /// ACT
    final result = AuthModel.fromMap(jsonMap);

    /// ASSERT
    expect(result, authResponse);
  });

  test('should map data json', () {
    /// ARRANGE
    const authModel = AuthModel(
        token: 'token',
        userId: 1,
        firstName: 'toto',
        lastName: 'tata',
        phoneNumber: '0101020203',
        emailAddress: 'test@gmail.com');
    Map<String, dynamic> authModelJson = {
      "token": 'token',
      "userId": 1,
      "firstName": 'toto',
      "lastName": 'tata',
      "phoneNumber": '0101020203',
      "emailAddress": 'test@gmail.com'
    };

    /// ACT
    final result = authModel.toMap();

    /// ASSERT
    expect(result, authModelJson);
  });
}
