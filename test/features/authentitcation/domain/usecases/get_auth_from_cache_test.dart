import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:project_bloc_clean/core/error/failure.dart';
import 'package:project_bloc_clean/core/usecases/usecases.dart';
import 'package:project_bloc_clean/features/authentication/domain/entities/auth.dart';
import 'package:project_bloc_clean/features/authentication/domain/repositories/auth_repository.dart';
import 'package:project_bloc_clean/features/authentication/domain/usecases/get_auth_from_cache.dart';

import 'sign_in_test.mocks.dart';

@GenerateMocks([AuthRepository]) //TODO flutter pub run build_runner build
void main() {
  late GetAuthFromCache usecase;
  late MockAuthRepository mockAuthRepository;

  setUp(() {
    mockAuthRepository = MockAuthRepository();
    usecase = GetAuthFromCache(mockAuthRepository);
  });

  test('should get the auth from the repository', () async {
    Right<Failure, Auth> getAuthResponse = const Right<Failure, Auth>(Auth(
        token: 'token',
        userId: 1,
        firstName: 'toto',
        lastName: 'tat',
        phoneNumber: '0101020203',
        emailAddress: 'test@gmail.com'));

    /// ARRANGE
    when(mockAuthRepository.getAuthFromCache())
        .thenAnswer((realInvocation) async => getAuthResponse);

    /// ACT
    final result =
        await usecase(NoParams()); //TODO comment éviter de mettre un param ici

    /// EXPECT
    expect(result, getAuthResponse);
    verify(mockAuthRepository.getAuthFromCache());
    verifyNoMoreInteractions(mockAuthRepository);
  });

  test('should return a value from map', () => null);
}
