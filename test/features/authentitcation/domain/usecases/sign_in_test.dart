import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:project_bloc_clean/core/error/failure.dart';
import 'package:project_bloc_clean/features/authentication/data/models/signin_payload.dart';
import 'package:project_bloc_clean/features/authentication/domain/entities/auth.dart';
import 'package:project_bloc_clean/features/authentication/domain/repositories/auth_repository.dart';
import 'package:project_bloc_clean/features/authentication/domain/usecases/sign_in.dart';
import 'package:mockito/annotations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'sign_in_test.mocks.dart';

@GenerateMocks([AuthRepository]) //TODO flutter pub run build_runner build
void main() {
  late SigninUsecaseImpl usecase;
  late MockAuthRepository mockAuthRepository;

  setUp(() {
    mockAuthRepository = MockAuthRepository();
    usecase = SigninUsecaseImpl(mockAuthRepository);
  });

  test('should get the signin from the repository', () async {
    final signinPayload =
        SigninPayload(email: 'test@gmail.com', password: 'test');

    Right<Failure, Auth> singinResponse = const Right<Failure, Auth>(Auth(
        token: 'token',
        userId: 1,
        firstName: 'toto',
        lastName: 'tat',
        phoneNumber: '0101020203',
        emailAddress: 'test@gmail.com'));

    /// ARRANGE
    when(mockAuthRepository.signIn(signinPayload))
        .thenAnswer((_) async => singinResponse);

    /// ACT
    final result = await usecase(signinPayload); // importance du `call`

    /// EXPECT
    expect(result, singinResponse);
    verify(mockAuthRepository.signIn(signinPayload));
    verifyNoMoreInteractions(mockAuthRepository);
  });
}
